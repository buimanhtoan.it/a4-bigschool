﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A4BIGSCHOOL.Startup))]
namespace A4BIGSCHOOL
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
