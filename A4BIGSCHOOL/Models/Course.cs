﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace A4BIGSCHOOL.Models
{
    public class Course
    {
        public int Id { get; set; }
        public ApplicationUser Lecturer { get; set; }
    }
}